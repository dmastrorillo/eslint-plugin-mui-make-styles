/**
 * @fileoverview eslint plugin to detect unused classes in material-ui&#39;s makeStyles and to detect references to non-existant styles used within your code
 * @author Daniel Mastrorillo
 */
"use strict";

//------------------------------------------------------------------------------
// Requirements
//------------------------------------------------------------------------------

var requireIndex = require("requireindex");

//------------------------------------------------------------------------------
// Plugin Definition
//------------------------------------------------------------------------------


// import all rules in lib/rules
module.exports.rules = requireIndex(__dirname + "/rules");



